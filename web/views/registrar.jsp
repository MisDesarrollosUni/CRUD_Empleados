<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<html lang="es">

<head>
    <meta charset="utf-8">
    <meta content="width=device-width, initial-scale=1.0" name="viewport">
    <title>CRUD | Registrar</title>
    <meta content="description" name="CRUD | Empleados">
    <meta content="author" name="UTEZ 2020">
    <link href="${pageContext.request.contextPath}/assets/img/favicon.svg" rel="icon">
    <link href="https://fonts.googleapis.com/css?family=Open+Sans:300,300i,400,400i,600,600i,700,700i|Raleway:300,300i,400,400i,500,500i,600,600i,700,700i|Poppins:300,300i,400,400i,500,500i,600,600i,700,700i"
            rel="stylesheet">
    <link href="${pageContext.request.contextPath}/assets/vendor/bootstrap/css/bootstrap.min.css" rel="stylesheet">
    <link href="${pageContext.request.contextPath}/assets/vendor/icofont/icofont.min.css" rel="stylesheet">
    <link href="${pageContext.request.contextPath}/assets/vendor/boxicons/css/boxicons.min.css" rel="stylesheet">
    <link href="${pageContext.request.contextPath}/assets/vendor/venobox/venobox.css" rel="stylesheet">
    <link href="${pageContext.request.contextPath}/assets/vendor/owl.carousel/assets/owl.carousel.min.css" rel="stylesheet">
    <link href="${pageContext.request.contextPath}/assets/vendor/aos/aos.css" rel="stylesheet">
    <link href="${pageContext.request.contextPath}/assets/css/style.css" rel="stylesheet">
    <link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/1.10.21/css/dataTables.bootstrap4.min.css">
    <link rel="stylesheet" type="text/css"
          href="https://cdn.datatables.net/responsive/2.2.5/css/responsive.bootstrap4.min.css">
</head>
<body>
<button type="button" class="mobile-nav-toggle d-xl-none"><i class="icofont-navigation-menu"></i></button>
<header id="header" class="d-flex flex-column justify-content-center">
    <nav class="nav-menu">
        <ul>
            <li class="active"><a href="${pageContext.request.contextPath}/index.jsp"><i class="bx bx-home"></i> <span>Inicio</span></a></li>
            <li><a href="${pageContext.request.contextPath}/search?accion=registrar"><i class="bx bx-user"></i> <span>Registrar</span></a></li>
            <li><a href="${pageContext.request.contextPath}/search?accion=consultar"><i class="bx bx-search-alt"></i> <span>Consultar</span></a></li>
            <li><a href="${pageContext.request.contextPath}/views/eliminar.jsp"><i class="bx bx-trash-alt"></i> <span>Eliminar</span></a></li>
        </ul>
    </nav>
</header>

<main id="main">
    <section id="about" class="about">
        <div class="container" data-aos="fade-up">
            <div class="section-title">
                <h2>Registrar Empleado</h2>
            </div>
            <form id="formInsert" action="${pageContext.request.contextPath}/insert" enctype="multipart/form-data" method="post">
                <div class="form-row">
                    <div class="col-md-4 mb-3">
                        <label for="nombre">Nombre</label>
                        <input placeholder="Ej: Paco Memo" type="text" class="form-control" id="nombre" name="nombre" maxlength="30" aria-describedby="validacionFeedback1" required>
                        <div hidden id="validacionFeedback1" class="invalid-feedback">
                            El nombre contiene carácteres inválidos o esta vacío
                        </div>
                    </div>

                    <div class="col-md-4 mb-3">
                        <label for="primerApellido">Primer apellido</label>
                        <input placeholder="Ej: Ochoa" type="text" class="form-control" id="primerApellido" name="primerApellido" maxlength="30" aria-describedby="validacionFeedback2" required>
                        <div hidden id="validacionFeedback2" class="invalid-feedback">
                            El apellido contiene carácteres inválidos o esta vacío
                        </div>
                    </div>

                    <div class="col-md-4 mb-3">
                        <label for="segundoApellido">Segundo apellido</label>
                        <input placeholder="Ej: Moreno" type="text" class="form-control" id="segundoApellido" name="segundoApellido" maxlength="30" aria-describedby="validacionFeedback3" required>
                        <div hidden id="validacionFeedback3" class="invalid-feedback">
                            El apellido contiene carácteres inválidos o esta vacío
                        </div>
                    </div>
                </div>

                <div class="form-row">
                    <div class="col-md-4 mb-3">
                        <label for="fechaNacimiento">Fecha de nacimiento</label>
                        <input type="date" class="form-control" id="fechaNacimiento" name="fechaNacimiento" aria-describedby="validacionFeedback4" required>
                        <div hidden id="validacionFeedback4" class="invalid-feedback">
                            Coloque su fecha de nacimiento o sea mayor de edad
                        </div>
                    </div>

                    <div class="col-md-3 mb-3">
                        <label for="departamento">Departamento</label>
                        <select class="custom-select" id="departamento" name="departamento" required aria-describedby="validacionFeedback5">
                            <option selected>Seleccione...</option>
                             <c:forEach var="dep" items="${listDep}">
                                 <option value="<c:out value="${dep.nombre}"/>"><c:out value="${dep.nombre}"/></option>
                             </c:forEach>
                        </select>
                        <div hidden id="validacionFeedback5" class="invalid-feedback">
                            Coloque un apartamento válido
                        </div>
                    </div>

                    <div class="col-md-5 mb-3">
                        <div class="form-group custom-file">
                        <label for="foto">Foto</label>
                            <input type="file" accept="image/*" class="form-control-file" name="foto" id="foto"
                                   required aria-describedby="validacionFeedback6">
                        </div>
                    </div>
                </div>
                <div class="container col-md-3">
                    <button class="btn btn-primary btn-block" type="button" onclick="confirmar()">Añadir</button>
                </div>
                <input type="text" value="insertar" name="accion" hidden>
            </form>
        </div>
    </section>
</main>

<a href="#" class="back-to-top"><i class="bx bx-up-arrow-alt"></i></a>
<div id="preloader"></div>
<script src="${pageContext.request.contextPath}/assets/vendor/jquery/jquery.min.js"></script>
<script src="https://code.jquery.com/jquery-3.5.1.js" integrity="sha256-QWo7LDvxbWT2tbbQ97B53yJnYU3WhH/C8ycbRAkjPDc=" crossorigin="anonymous"></script>
<script src="${pageContext.request.contextPath}/assets/vendor/bootstrap/js/bootstrap.bundle.min.js"></script>
<script src="${pageContext.request.contextPath}/assets/vendor/jquery.easing/jquery.easing.min.js"></script>
<script src="${pageContext.request.contextPath}/assets/vendor/php-email-form/validate.js"></script>
<script src="${pageContext.request.contextPath}/assets/vendor/waypoints/jquery.waypoints.min.js"></script>
<script src="${pageContext.request.contextPath}/assets/vendor/counterup/counterup.min.js"></script>
<script src="${pageContext.request.contextPath}/assets/vendor/isotope-layout/isotope.pkgd.min.js"></script>
<script src="${pageContext.request.contextPath}/assets/vendor/venobox/venobox.min.js"></script>
<script src="${pageContext.request.contextPath}/assets/vendor/owl.carousel/owl.carousel.min.js"></script>
<script src="${pageContext.request.contextPath}/assets/vendor/typed.js/typed.min.js"></script>
<script src="${pageContext.request.contextPath}/assets/vendor/aos/aos.js"></script>
<script src="${pageContext.request.contextPath}/assets/js/main.js"></script>
<script src="https://cdn.jsdelivr.net/npm/sweetalert2@9"></script>
<script src="${pageContext.request.contextPath}/assets/js/validaciones.js"></script>
</body>
</html>
