<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<html lang="es">

<head>
	<meta charset="utf-8">
	<meta content="width=device-width, initial-scale=1.0" name="viewport">
	<title>CRUD | Consultar</title>
	<meta content="description" name="CRUD | Empleados">
	<meta content="author" name="UTEZ 2020">
	<link href="${pageContext.request.contextPath}/assets/img/favicon.svg" rel="icon">
	<link href="https://fonts.googleapis.com/css?family=Open+Sans:300,300i,400,400i,600,600i,700,700i|Raleway:300,300i,400,400i,500,500i,600,600i,700,700i|Poppins:300,300i,400,400i,500,500i,600,600i,700,700i"
	      rel="stylesheet">
	<link href="${pageContext.request.contextPath}/assets/vendor/bootstrap/css/bootstrap.min.css" rel="stylesheet">
	<link href="${pageContext.request.contextPath}/assets/vendor/icofont/icofont.min.css" rel="stylesheet">
	<link href="${pageContext.request.contextPath}/assets/vendor/boxicons/css/boxicons.min.css" rel="stylesheet">
	<link href="${pageContext.request.contextPath}/assets/vendor/venobox/venobox.css" rel="stylesheet">
	<link href="${pageContext.request.contextPath}/assets/vendor/owl.carousel/assets/owl.carousel.min.css"
	      rel="stylesheet">
	<link href="${pageContext.request.contextPath}/assets/vendor/aos/aos.css" rel="stylesheet">
	<link href="${pageContext.request.contextPath}/assets/css/style.css" rel="stylesheet">
	<link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/1.10.21/css/dataTables.bootstrap4.min.css">
	<link rel="stylesheet" type="text/css"
	      href="https://cdn.datatables.net/responsive/2.2.5/css/responsive.bootstrap4.min.css">
</head>

<body>
<button type="button" class="mobile-nav-toggle d-xl-none"><i class="icofont-navigation-menu"></i></button>
<header id="header" class="d-flex flex-column justify-content-center">
	<nav class="nav-menu">
		<ul>
			<li class="active"><a href="${pageContext.request.contextPath}/index.jsp"><i class="bx bx-home"></i> <span>Inicio</span></a>
			</li>
			<li><a href="${pageContext.request.contextPath}/search?accion=registrar"><i class="bx bx-user"></i> <span>Registrar</span></a>
			</li>
			<li><a href="${pageContext.request.contextPath}/search?accion=consultar"><i class="bx bx-search-alt"></i><span>Consultar</span></a>
			</li>
			<li><a href="${pageContext.request.contextPath}/views/eliminar.jsp"><i class="bx bx-trash-alt"></i> <span>Eliminar</span></a></li>
		</ul>
	</nav>
</header>

<main id="main">
	<section id="about" class="about">
		<div class="container" data-aos="fade-up">

			<div class="section-title">
				<h2>Consultar Empleado</h2>
			</div>
			<table id="datatable" class="table table-bordered text-center display responsive nowrap" width="100%">
				<thead class="thead-light">
				<tr>
					<th>Nombre</th>
					<th>Primer apellido</th>
					<th>Segundo apellido</th>
					<th>Edad</th>
					<th>Departamento</th>
					<th>Ver foto</th>
					<th>Modificar</th>
				</tr>
				</thead>
				<tbody>
				<c:forEach var="emp" items="${listEmp}">
					<tr>
						<td><c:out value="${emp.nombre}"/></td>
						<td><c:out value="${emp.primerApellido}"/></td>
						<td><c:out value="${emp.segundoApellido}"/></td>
						<td><c:out value="${emp.edad}"/></td>
						<td><c:out value="${emp.idDep.nombre}"/></td>
						<td>
							<button class="btn btn-block btn btn-outline-success" onclick="verFoto(`<c:out value="${emp.id}"/>`)" data-toggle="modal"
							        data-target="#exampleModal">
								<svg width="1em" height="1em" viewBox="0 0 16 16" class="bi bi-image-fill"
								     fill="currentColor" xmlns="http://www.w3.org/2000/svg">
									<path fill-rule="evenodd"
									      d="M.002 3a2 2 0 0 1 2-2h12a2 2 0 0 1 2 2v10a2 2 0 0 1-2 2h-12a2 2 0 0 1-2-2V3zm1 9l2.646-2.354a.5.5 0 0 1 .63-.062l2.66 1.773 3.71-3.71a.5.5 0 0 1 .577-.094L15.002 9.5V13a1 1 0 0 1-1 1h-12a1 1 0 0 1-1-1v-1zm5-6.5a1.5 1.5 0 1 1-3 0 1.5 1.5 0 0 1 3 0z"/>
								</svg>
							</button>
						</td>
						<td>
							<button class="btn btn-block btn btn-outline-info" data-toggle="modal"
							        onclick="editar(`<c:out value="${emp.id}"/>`,`<c:out value="${emp.nombre}"/>`,`<c:out value="${emp.primerApellido}"/>`,`<c:out value="${emp.segundoApellido}"/>`,`<c:out value="${emp.fechaN}"/>`,`<c:out value="${emp.idDep.nombre}"/>`)" data-target="#exampleModal2">
								<svg width="1em" height="1em" viewBox="0 0 16 16" class="bi bi-pencil-square"
								     fill="currentColor" xmlns="http://www.w3.org/2000/svg">
									<path d="M15.502 1.94a.5.5 0 0 1 0 .706L14.459 3.69l-2-2L13.502.646a.5.5 0 0 1 .707 0l1.293 1.293zm-1.75 2.456l-2-2L4.939 9.21a.5.5 0 0 0-.121.196l-.805 2.414a.25.25 0 0 0 .316.316l2.414-.805a.5.5 0 0 0 .196-.12l6.813-6.814z"/>
									<path fill-rule="evenodd"
									      d="M1 13.5A1.5 1.5 0 0 0 2.5 15h11a1.5 1.5 0 0 0 1.5-1.5v-6a.5.5 0 0 0-1 0v6a.5.5 0 0 1-.5.5h-11a.5.5 0 0 1-.5-.5v-11a.5.5 0 0 1 .5-.5H9a.5.5 0 0 0 0-1H2.5A1.5 1.5 0 0 0 1 2.5v11z"/>
								</svg>
							</button>
						</td>
					</tr>
				</c:forEach>
				</tbody>
			</table>
		</div>
	</section>
</main>

<div class="modal fade" id="exampleModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel"
     aria-hidden="true">
	<div class="modal-dialog" role="document">
		<div class="modal-content">
			<div class="modal-header">
				<h5 class="modal-title" id="exampleModalLabel">Foto</h5>
				<button type="button" class="close" data-dismiss="modal" aria-label="Close">
					<span aria-hidden="true">&times;</span>
				</button>
			</div>
			<div class="modal-body">
				<img id="imgFoto" class="img-fluid rounded mx-auto d-block">
			</div>
			<div class="modal-footer">
				<button type="button" class="btn btn-success" data-dismiss="modal">Cerrar</button>
			</div>
		</div>
	</div>
</div>

<div class="modal fade" id="exampleModal2" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel"
     aria-hidden="true">
	<div class="modal-dialog" role="document">
		<div class="modal-content">
			<div class="modal-header">
				<h5 class="modal-title" id="exampleModalLabel2">Editar Datos</h5>
				<button type="button" class="close" data-dismiss="modal" aria-label="Close">
					<span aria-hidden="true">&times;</span>
				</button>
			</div>
			<form id="formUpdate" action="${pageContext.request.contextPath}/update" method="post" enctype="multipart/form-data">
				<div class="modal-body">
					<input type="text" hidden id="idEmp" name="idEmp">
					<fieldset class="form-group">
						<label for="nombre">Nombre</label>
						<input placeholder="Ej: Paco Memo" type="text" class="form-control" id="nombre" name="nombre" maxlength="30" aria-describedby="validacionFeedback1" required>
						<div hidden id="validacionFeedback1" class="invalid-feedback">
							El nombre contiene carácteres inválidos o esta vacío
						</div>
					</fieldset>
					<fieldset class="form-group">
						<label for="primerApellido">Primer apellido</label>
						<input placeholder="Ej: Ochoa" type="text" class="form-control" id="primerApellido" name="primerApellido" maxlength="30" aria-describedby="validacionFeedback2" required>
						<div hidden id="validacionFeedback2" class="invalid-feedback">
							El apellido contiene carácteres inválidos o esta vacío
						</div>
					</fieldset>
					<fieldset class="form-group">
						<label for="segundoApellido">Segundo apellido</label>
						<input placeholder="Ej: Moreno" type="text" class="form-control" id="segundoApellido" name="segundoApellido" maxlength="30" aria-describedby="validacionFeedback3" required>
						<div hidden id="validacionFeedback3" class="invalid-feedback">
							El apellido contiene carácteres inválidos o esta vacío
						</div>
					</fieldset>
					<fieldset class="form-group">
						<label for="fechaNacimiento">Fecha de nacimiento</label>
						<input type="date" class="form-control" id="fechaNacimiento" name="fechaNacimiento" aria-describedby="validacionFeedback4" required>
						<div hidden id="validacionFeedback4" class="invalid-feedback">
							Coloque su fecha de nacimiento o sea mayor de edad
						</div>
					</fieldset>
					<fieldset class="form-group">
						<label for="departamento">Departamento</label>
						<select class="custom-select" id="departamento" name="departamento" required aria-describedby="validacionFeedback5">
							<option selected>Seleccione...</option>
							<c:forEach var="dep" items="${listDep}">
								<option value="<c:out value="${dep.nombre}"/>"><c:out value="${dep.nombre}"/></option>
							</c:forEach>
						</select>
						<div hidden id="validacionFeedback5" class="invalid-feedback">
							Coloque un apartamento válido
						</div>
					</fieldset>
					<fieldset class="form-group">
						<div class="col-md-5 mb-3">
							<div class="form-group custom-file">
								<label for="foto">Foto</label>
								<input type="file" accept="image/*" class="form-control-file" name="foto" id="foto"
								       required aria-describedby="validacionFeedback6">
							</div>
						</div>
					</fieldset>
				</div>
				<div class="modal-footer">
					<input value="update" name="accion" hidden>
					<button type="button" class="btn btn-secondary" data-dismiss="modal">Cerrar</button>
					<button type="button" class="btn btn-info" onclick="modificar()">Modificar</button>
				</div>
			</form>
		</div>
	</div>
</div>

<div id="preloader"></div>
<script src="${pageContext.request.contextPath}/assets/vendor/jquery/jquery.min.js"></script>
<script src="${pageContext.request.contextPath}/assets/vendor/bootstrap/js/bootstrap.bundle.min.js"></script>
<script src="${pageContext.request.contextPath}/assets/vendor/jquery.easing/jquery.easing.min.js"></script>
<script src="${pageContext.request.contextPath}/assets/vendor/php-email-form/validate.js"></script>
<script src="${pageContext.request.contextPath}/assets/vendor/waypoints/jquery.waypoints.min.js"></script>
<script src="${pageContext.request.contextPath}/assets/vendor/counterup/counterup.min.js"></script>
<script src="${pageContext.request.contextPath}/assets/vendor/isotope-layout/isotope.pkgd.min.js"></script>
<script src="${pageContext.request.contextPath}/assets/vendor/venobox/venobox.min.js"></script>
<script src="${pageContext.request.contextPath}/assets/vendor/owl.carousel/owl.carousel.min.js"></script>
<script src="${pageContext.request.contextPath}/assets/vendor/typed.js/typed.min.js"></script>
<script src="${pageContext.request.contextPath}/assets/vendor/aos/aos.js"></script>
<script src="${pageContext.request.contextPath}/assets/js/main.js"></script>
<script type="text/javascript" charset="utf8" src="https://cdn.datatables.net/1.10.21/js/jquery.dataTables.min.js"></script>
<script type="text/javascript" charset="utf8" src="https://cdn.datatables.net/1.10.21/js/dataTables.bootstrap4.min.js"></script>
<script type="text/javascript" charset="utf8" src="https://cdn.datatables.net/responsive/2.2.5/js/dataTables.responsive.min.js"></script>
<script type="text/javascript" charset="utf8" src="https://cdn.datatables.net/responsive/2.2.5/js/responsive.bootstrap4.min.js"></script>
<script src="https://cdn.jsdelivr.net/npm/sweetalert2@9"></script>
<script type="text/javascript" src="${pageContext.request.contextPath}/assets/js/Funciones.js"></script>

<script type="text/javascript">

	function editar(a, b, c, d, e, f) {
		console.log(a, b, c, d, e, f);
		document.getElementById("idEmp").value = a;
		document.getElementById("nombre").value = b;
		document.getElementById("primerApellido").value = c;
		document.getElementById("segundoApellido").value = d;
		document.getElementById("fechaNacimiento").value = e;
		document.getElementById("departamento").value = f;
	}

	function eliminarSMS(id) {
		document.getElementById("enviarDelete").href = "${pageContext.request.contextPath}/delete?idEmp=" + id + "&accion=delete";
		Swal.fire({
			title: '¿Estás seguro?', text: "¡Todos los datos se eliminarán!",
			icon: 'warning',
			showCancelButton: true,
			confirmButtonColor: '#20a2a7',
			cancelButtonColor: '#d33',
			confirmButtonText: '¡Si, eliminalo!'
		}).then((result) => {
			if (result.value) {
				Swal.fire({
					position: 'center',
					icon: 'success',
					title: '¡Eliminado!',
					text: 'Los datos se eliminaron correctamente',
					showConfirmButton: false,
					timer: 1500
				})
				setTimeout(function () {
					document.getElementById("enviarDelete").click();
				}, 1000)
			}
		})
	}

	function verFoto(id) {
		document.getElementById("imgFoto").src = "${pageContext.request.contextPath}/imagen?idEmp=" + id + "&accion=verFoto";
	}

</script>
<script src="${pageContext.request.contextPath}/assets/js/validaciones.js"></script>

</body>
</html>