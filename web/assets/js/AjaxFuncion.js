(function () {
    consulta();
}())

function consulta() {
    const url = "http://localhost:8080/CRUD_Empleados/ListaAjax";
    fetch(url).then(function (response) {
        return response.text();
    }).then(function (data) {
        let tabla = document.getElementById("datatable");
        tabla.innerHTML = " <thead>\n" +
            "        <tr>\n" +
            "            <th>Nombre</th>\n" +
            "            <th>Primer Apellido</th>\n" +
            "            <th>Segundo Apellido</th>\n" +
            "            <th>Eliminar</th>\n" +
            "        </tr>\n" +
            "        </thead>";
        var obj = JSON.parse(data);
        for (var i in obj){
            tabla.insertRow(-1).innerHTML=
                '<td>' + obj[i].nombre + '</td>' +
                '<td>' + obj[i].primerApellido + '</td>' +
                '<td>' + obj[i].segundoApellido + '</td>' +
                '<td><button class="btn btn-block btn btn-outline-danger" onclick="eliminar(' + obj[i].id + ')">\n' +
                '<svg class="bi bi-trash" width="1em" height="1em" viewBox="0 0 16 16" fill="currentColor" xmlns="http://www.w3.org/2000/svg">\n' +
                '<path d="M5.5 5.5A.5.5 0 0 1 6 6v6a.5.5 0 0 1-1 0V6a.5.5 0 0 1 .5-.5zm2.5 0a.5.5 0 0 1 .5.5v6a.5.5 0 0 1-1 0V6a.5.5 0 0 1 .5-.5zm3 .5a.5.5 0 0 0-1 0v6a.5.5 0 0 0 1 0V6z"/>\n' +
                '<path fill-rule="evenodd" d="M14.5 3a1 1 0 0 1-1 1H13v9a2 2 0 0 1-2 2H5a2 2 0 0 1-2-2V4h-.5a1 1 0 0 1-1-1V2a1 1 0 0 1 1-1H6a1 1 0 0 1 1-1h2a1 1 0 0 1 1 1h3.5a1 1 0 0 1 1 1v1zM4.118 4L4 4.059V13a1 1 0 0 0 1 1h6a1 1 0 0 0 1-1V4.059L11.882 4H4.118zM2.5 3V2h11v1h-11z"/>\n' +
                '</svg>\n' +
                '</button>\n</td>';
        }
    }).catch(function (err) {
        console.log(err);
    });
}


function eliminar(id) {
    console.log(id);
    Swal.fire({
        title: '¿Estás seguro?', text: "¡Todos los datos se eliminarán!",
        icon: 'warning',
        showCancelButton: true,
        confirmButtonColor: '#20a2a7',
        cancelButtonColor: '#d33',
        confirmButtonText: '¡Si, eliminalo!'
    }).then((result) => {
        if (result.value) {
            const url = "http://localhost:8080/CRUD_Empleados/ListaAjax";
            axios({
                method: 'post',
                url: url,
                params: {
                    id: id
                }
            }).then(response=>{
                Swal.fire({
                    position: 'center',
                    icon: 'success',
                    title: '¡Eliminado!',
                    text: 'Los datos se eliminaron correctamente',
                    showConfirmButton: false,
                    timer: 1500
                })
                consulta();
            }).catch(e=>{
                Swal.fire({
                    position: 'center',
                    icon: 'error',
                    title: '¡Ocurrio Algo!',
                    text: 'Los datos no se eliminaron',
                    showConfirmButton: false,
                    timer: 1500
                })
            });
        }
    })
}