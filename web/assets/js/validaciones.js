const CARACTERES = 'áéíóúÁÉÍÓÚabcdefghijklmnñopqrstuvwxyzABCDEFGHIJKLMNÑOPQRSTUVWXYZ. ';

async function confirmar() {
	let datos = [await funNombre(),await funPrimerApellido(),await funSegundoApellido(),await funFechaN(),await funDep(),await funFoto()];
	let valido = datos[0]&&datos[1]&&datos[2]&&datos[3]&&datos[4]&&datos[5];
	if (valido){
		Swal.fire({
			position: 'center',
			icon: 'success',
			title: '¡Empleado registrado!',
			text: '¡Se registro con éxito en el sistema!',
			showConfirmButton: false,
			timer: 1500
		})
		setTimeout(function () {
			document.getElementById("formInsert").submit();
		},1000)

	}else {
		Swal.fire(
			'¡Verifique todos sus datos!', '¡Todos los campos deben ser llenados!', 'error'
		)
	}
}

async function modificar() {
	let datos = [await funNombre(),await funPrimerApellido(),await funSegundoApellido(),await funFechaN(),await funDep()];
	let valido = datos[0]&&datos[1]&&datos[2]&&datos[3]&&datos[4];
	console.log(valido);
	if (valido){
		Swal.fire({
			title: '¿Estás seguro?',
			text: "¡Todos los datos se modificarán!",
			icon: 'warning',
			showCancelButton: true,
			confirmButtonColor: '#20a2a7',
			cancelButtonColor: '#d33',
			confirmButtonText: '¡Si, modificalo!'
		}).then((result) => {
			if (result.value) {
				Swal.fire({
					position: 'center',
					icon: 'success',
					title: '¡Modificado!',
					text: 'Los datos se modificaron correctamente',
					showConfirmButton: false,
					timer: 1500
				})

				setTimeout(function () {
					document.getElementById("formUpdate").submit();
				}, 1000)
			}
		})
	}else {
		Swal.fire(
			'¡Verifique todos sus datos!', '¡Todos los campos deben ser llenados!', 'error'
		)
	}
}


function funNombre() {
	return new Promise(retornar=>{
		let input = document.getElementById("nombre");
		let valor = input.value;
		let flag = true;
		if(valor.length > 0){
			for (let i = 0; i < valor.length; i++) {
				if (CARACTERES.indexOf(valor.charAt(i))==-1){
					flag = false;
					break;
				}
			}
			if(flag){
				input.classList.remove("is-invalid");
				input.classList.add("is-valid");
				retornar(true)

			}else{
				input.classList.remove("is-valid");
				input.classList.add("is-invalid");
				document.getElementById("validacionFeedback1").hidden=false;
				retornar(false)
			}
		}else{
			input.classList.remove("is-valid");
			input.classList.add("is-invalid");
			document.getElementById("validacionFeedback1").hidden=false;
			retornar(false);
		}
	})
}

function funPrimerApellido() {
	return new Promise(retornar=>{
		let input = document.getElementById("primerApellido");
		let valor = input.value;
		let flag = true;
		if(valor.length>0){
			for (let i = 0; i < valor.length; i++) {
				if (CARACTERES.indexOf(valor.charAt(i))==-1){
					flag = false;
					break;
				}
			}
			if(flag){
				input.classList.remove("is-invalid");
				input.classList.add("is-valid");
				retornar(true)

			}else{
				input.classList.remove("is-valid");
				input.classList.add("is-invalid");
				document.getElementById("validacionFeedback2").hidden=false;
				retornar(false)
			}
		}else{
			input.classList.remove("is-valid");
			input.classList.add("is-invalid");
			document.getElementById("validacionFeedback2").hidden=false;
			retornar(false);
		}
	})
}

function funSegundoApellido() {
	return new Promise(retornar=>{
		let input = document.getElementById("segundoApellido");
		let valor = input.value;
		let flag = true;
		if(valor.length>0){
			for (let i = 0; i < valor.length; i++) {
				if (CARACTERES.indexOf(valor.charAt(i))==-1){
					flag = false;
					break;
				}
			}
			if(flag){
				input.classList.remove("is-invalid");
				input.classList.add("is-valid");
				retornar(true)

			}else{
				input.classList.remove("is-valid");
				input.classList.add("is-invalid");
				document.getElementById("validacionFeedback3").hidden=false;
				retornar(false)
			}
		}else{
			input.classList.remove("is-valid");
			input.classList.add("is-invalid");
			document.getElementById("validacionFeedback3").hidden=false;
			retornar(false);
		}

	})
}





/*function funNombre() {
	return new Promise(retornar=>{
		let input = document.getElementById("nombre");
		let valor = input.value;
		console.log(valor.length);
		if(valor.length > 0){
			for (let i = 0; i < valor.length; i++) {
				if (CARACTERES.indexOf(valor.charAt(i))!=-1){
					input.classList.remove("is-invalid");
					input.classList.add("is-valid");
					retornar(true);
				}else {
					input.classList.remove("is-valid");
					input.classList.add("is-invalid");
					document.getElementById("validacionFeedback1").hidden=false;
					retornar(false);
				}
			}
		}else{
			input.classList.remove("is-valid");
			input.classList.add("is-invalid");
			document.getElementById("validacionFeedback1").hidden=false;
			retornar(false);
		}
	})
}

function funPrimerApellido() {
	return new Promise(retornar=>{
		let input = document.getElementById("primerApellido");
		let valor = input.value;
		if(valor.length>0){
			for (let i = 0; i < valor.length; i++) {
				if (CARACTERES.indexOf(valor.charAt(i))!=-1){
					input.classList.remove("is-invalid");
					input.classList.add("is-valid");
					retornar(true);
				}else {
					input.classList.remove("is-valid");
					input.classList.add("is-invalid");
					document.getElementById("validacionFeedback2").hidden=false;
					retornar(false);
				}
			}
		}else{
			input.classList.remove("is-valid");
			input.classList.add("is-invalid");
			document.getElementById("validacionFeedback2").hidden=false;
			retornar(false);
		}
	})
}

function funSegundoApellido() {
	return new Promise(retornar=>{
		let input = document.getElementById("segundoApellido");
		let valor = input.value;
		if(valor.length>0){
			for (let i = 0; i < valor.length; i++) {
				if (CARACTERES.indexOf(valor.charAt(i))!=-1){
					input.classList.remove("is-invalid");
					input.classList.add("is-valid");
					retornar(true);
				}else {
					input.classList.remove("is-valid");
					input.classList.add("is-invalid");
					document.getElementById("validacionFeedback3").hidden=false;
					retornar(false);
				}
			}
		}else{
			input.classList.remove("is-valid");
			input.classList.add("is-invalid");
			document.getElementById("validacionFeedback3").hidden=false;
			retornar(false);
		}

	})
}*/

function funFechaN() {
	return new Promise(retornar=>{
		let input = document.getElementById("fechaNacimiento");
		let valor = input.value;
		let numero = Number(valor.substring(0,4));
		if (valor!= '' && numero<=2002){
			input.classList.remove("is-invalid");
			input.classList.add("is-valid");
			retornar(true);
		}else {
			input.classList.remove("is-valid");
			input.classList.add("is-invalid");
			document.getElementById("validacionFeedback4").hidden=false;
			retornar(false);
		}
	})
}

function funDep() {
	return new Promise(retornar=>{
		let input = document.getElementById("departamento");
		let valor= input.value;
		if (valor!='Seleccione...'){
			input.classList.remove("is-invalid");
			input.classList.add("is-valid");
			retornar(true);
		}else {
			input.classList.remove("is-valid");
			input.classList.add("is-invalid");
			document.getElementById("validacionFeedback5").hidden=false;
			retornar(false);
		}
	})
}

function funFoto() {
	return new Promise(retornar=>{
		let input = document.getElementById("foto");
		let valor = input.value;
		let extencion = valor.substring(valor.length-4, valor.length);
		if (valor != ''){
			if(extencion == ".png" || extencion == ".jpg"){
				retornar(true);
			}else{
				retornar(false);
			}
		}else {
			retornar(false);
		}
	})
}



/*function funFoto() {
	return new Promise(retornar=>{
		let input = document.getElementById("foto");
		let valor = input.value;
		if (valor != ''){
			input.classList.remove("is-invalid");
			input.classList.add("is-valid");
			retornar(true);
		}else {
			input.classList.remove("is-valid");
			input.classList.add("is-invalid");
			retornar(false);
		}
	})
}*/