package mx.edu.utez.Utility;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;

public class Conexion {
    public Connection crearConexion() throws SQLException {
        Connection con;
        try{
            Class.forName("com.mysql.cj.jdbc.Driver").newInstance();
        }catch (Exception e){
            System.out.println(e);
        }
        con = DriverManager.getConnection("jdbc:mysql://bddf5087841378:b570abe8@us-cdbr-east-02.cleardb.com/heroku_404d82d98924fa5?reconnect=true&allowPublicKeyRetrieval=true&useSSL=false");
        return  con;
    }
}
