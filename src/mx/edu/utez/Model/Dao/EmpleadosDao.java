package mx.edu.utez.Model.Dao;

import mx.edu.utez.Model.Bean.DepartamentosBean;
import mx.edu.utez.Model.Bean.EmpleadosBean;
import mx.edu.utez.Utility.Conexion;

import javax.servlet.http.HttpServletResponse;
import java.io.BufferedInputStream;
import java.io.BufferedOutputStream;
import java.io.InputStream;
import java.io.OutputStream;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.List;

public class EmpleadosDao extends Conexion {

    public List<DepartamentosBean> searchDep() {
        List <DepartamentosBean> dep = new ArrayList<>();
        try{
            PreparedStatement pst = crearConexion().prepareStatement("select * from departamentos");
            ResultSet rs = pst.executeQuery();
            while (rs.next()){
                int id = rs.getInt("id");
                String nombre = rs.getString("nombre");
                dep.add(new DepartamentosBean(id,nombre));
            }
            pst.close();
            rs.close();
            crearConexion().close();
        }catch (Exception e){
            e.printStackTrace();
        }
        return dep;
    }

    public List<EmpleadosBean> searchEmp() {
        List <EmpleadosBean> emp = new ArrayList<>();
        try {
            PreparedStatement pst = crearConexion().prepareStatement("select e.idEmpleado,e.nombre,e.primerApellido,e.segundoApellido, e.fechaN,e.idDepartamento, d.nombre from empleados e join departamentos d on (e.idDepartamento=d.id)");
            ResultSet rs = pst.executeQuery();
            while (rs.next()){
                int idEmp = rs.getInt("e.idEmpleado");
                String nombre = rs.getString("e.nombre");
                String primerApellido = rs.getString("e.primerApellido");
                String segundoApellido = rs.getString("e.segundoApellido");
                String fechaN = rs.getString("e.fechaN");
                String dep = rs.getString("d.nombre");
                int edad = 2020-Integer.parseInt(fechaN.substring(0,4));
                emp.add(new EmpleadosBean(idEmp,nombre,primerApellido,segundoApellido,fechaN,new DepartamentosBean(dep),edad));
            }
            pst.close();
            rs.close();
            crearConexion().close();
        }catch (Exception e){
            e.printStackTrace();
        }
    return emp;
    }

    public boolean deleteEmp(EmpleadosBean empBean) {
        boolean flag = false;
        try{
            PreparedStatement pst = crearConexion().prepareStatement("delete from empleados where idEmpleado=?");
            pst.setInt(1,empBean.getId());
            if(pst.executeUpdate()>0){
                flag = true;
                System.out.println("Se elimino correctamente...");
            }else{
                System.out.println("Algo paso...");
            }
            pst.close();
            crearConexion().close();
        }catch (Exception e){
         e.printStackTrace();
        }
        return flag;
    }

    public void updateEmp(EmpleadosBean empBean) {
        try {
            PreparedStatement pst = crearConexion().prepareStatement("update empleados set nombre=?,primerApellido=?,segundoApellido=?,fechaN=?,idDepartamento =(select id from departamentos where nombre=?) where idEmpleado=?");
            pst.setString(1,empBean.getNombre());
            pst.setString(2,empBean.getPrimerApellido());
            pst.setString(3,empBean.getSegundoApellido());
            pst.setString(4,empBean.getFechaN());
            pst.setString(5,empBean.getIdDep().getNombre());
            pst.setInt(6,empBean.getId());
            if (pst.executeUpdate()>0){
                System.out.println("Se actualizo");
            }else {
                System.out.println("No sirvio");
            }
            pst.close();
            crearConexion().close();
        }catch (Exception e){
            e.printStackTrace();
        }
    }


    public void selectImagen(EmpleadosBean emp, HttpServletResponse response) {
        try{
            InputStream is = null;
            OutputStream os = null;
            BufferedOutputStream bos = null;
            BufferedInputStream bis = null;
            response.setContentType("image/*");
            os = response.getOutputStream();
            PreparedStatement pst = crearConexion().prepareStatement("select foto from empleados where idEmpleado=?");
            pst.setInt(1,emp.getId());
            ResultSet rs = pst.executeQuery();
            if (rs.next()){
                is = rs.getBinaryStream("foto");
            }
            bis = new BufferedInputStream(is);
            bos = new BufferedOutputStream(os);
            int i = 0;
            while ((i=bis.read())!=-1){
                bos.write(i);
            }
            bos.close();
            bis.close();
            rs.close();
            pst.close();
            crearConexion().close();
        }catch (Exception e){
            e.printStackTrace();
        }
    }

    public boolean insertEmp(EmpleadosBean empleadosBean){
        boolean flag = false;
        try{
            PreparedStatement pst = crearConexion().prepareStatement("insert into empleados (nombre,primerApellido,segundoApellido,fechaN,foto,idDepartamento) values (?,?,?,?,?,(select id from departamentos where nombre=?))");
            pst.setString(1,empleadosBean.getNombre());
            pst.setString(2,empleadosBean.getPrimerApellido());
            pst.setString(3,empleadosBean.getSegundoApellido());
            pst.setString(4,empleadosBean.getFechaN());
            pst.setBlob(5,empleadosBean.getFoto());
            pst.setString(6,empleadosBean.getIdDep().getNombre());
            if (pst.executeUpdate()>0){
                System.out.println("Si entro jajajaj");
                flag=true;
            }else {
                System.out.println("Ah bueno XDDD");
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return flag;
    }

    public void updateFoto(InputStream foto, int id) {
       try{
           PreparedStatement pst = crearConexion().prepareStatement("update empleados set foto=? where idEmpleado=?");
           pst.setBlob(1,foto);
           pst.setInt(2,id);
           if (pst.executeUpdate()>0){
               System.out.println("Se realizo correctamente");
           }else{
               System.out.println("No se realizo");
           }
       }catch (Exception e){
           e.printStackTrace();
       }
    }
}
