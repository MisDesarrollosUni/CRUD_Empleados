package mx.edu.utez.Model.Bean;

public class DepartamentosBean {
    private int id;
    private String nombre;

    public DepartamentosBean() {
    }



    public DepartamentosBean(int id, String nombre) {
        this.id = id;
        this.nombre = nombre;
    }

    public DepartamentosBean(String nombre) {
        this.nombre = nombre;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }
}
