package mx.edu.utez.Model.Bean;

import java.io.InputStream;

public class EmpleadosBean {
    private int id;
    private String nombre;
    private String primerApellido;
    private String segundoApellido;
    private String fechaN;
    private DepartamentosBean idDep;
    private int edad;
    private InputStream foto;
    public EmpleadosBean() {
    }

    public EmpleadosBean(int id, String nombre, String primerApellido, String segundoApellido, String fechaN, DepartamentosBean idDep, int edad) {
        this.id = id;
        this.nombre = nombre;
        this.primerApellido = primerApellido;
        this.segundoApellido = segundoApellido;
        this.fechaN = fechaN;
        this.idDep = idDep;
        this.edad = edad;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public String getPrimerApellido() {
        return primerApellido;
    }

    public void setPrimerApellido(String primerApellido) {
        this.primerApellido = primerApellido;
    }

    public String getSegundoApellido() {
        return segundoApellido;
    }

    public void setSegundoApellido(String segundoApellido) {
        this.segundoApellido = segundoApellido;
    }

    public String getFechaN() {
        return fechaN;
    }

    public void setFechaN(String fechaN) {
        this.fechaN = fechaN;
    }

    public DepartamentosBean getIdDep() {
        return idDep;
    }

    public void setIdDep(DepartamentosBean idDep) {
        this.idDep = idDep;
    }

    public int getEdad() {
        return edad;
    }

    public void setEdad(int edad) {
        this.edad = edad;
    }

    public InputStream getFoto() {
        return foto;
    }

    public void setFoto(InputStream foto) {
        this.foto = foto;
    }
}
