package mx.edu.utez.Controller;
import com.google.gson.Gson;
import mx.edu.utez.Model.Bean.DepartamentosBean;
import mx.edu.utez.Model.Bean.EmpleadosBean;
import mx.edu.utez.Model.Dao.EmpleadosDao;

import javax.servlet.ServletException;
import javax.servlet.annotation.MultipartConfig;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.*;
import java.io.IOException;
import java.io.InputStream;
import java.util.List;
@MultipartConfig
@WebServlet(name = "EmpleadosServlet", urlPatterns = {"/insert","/update", "/delete", "/search","/imagen"})
public class EmpleadosServlet extends HttpServlet {
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");
        request.setCharacterEncoding("UTF-8");
        String accion = request.getParameter("accion");
        EmpleadosDao emp = new EmpleadosDao();
        EmpleadosBean empBean = new EmpleadosBean();

        switch (accion){
            case "update":
                try {
                    empBean.setId(Integer.parseInt(request.getParameter("idEmp")));
                    empBean.setNombre(request.getParameter("nombre"));
                    empBean.setPrimerApellido(request.getParameter("primerApellido"));
                    empBean.setSegundoApellido(request.getParameter("segundoApellido"));
                    empBean.setFechaN(request.getParameter("fechaNacimiento"));
                    empBean.setIdDep(new DepartamentosBean(request.getParameter("departamento")));
                    Part part = request.getPart("foto");
                    InputStream foto = part.getInputStream();
                    if (!foto.toString().contains("ByteArrayInputStream")){
                        emp.updateFoto(foto,empBean.getId());
                    }
                    emp.updateEmp(empBean);
                    List<DepartamentosBean> dep = emp.searchDep();
                    request.setAttribute("listDep",dep);
                    List<EmpleadosBean> empList = emp.searchEmp();
                    request.setAttribute("listEmp",empList);
                    request.getRequestDispatcher("/views/consultar.jsp").forward(request,response);
                }catch (Exception e){
                    e.printStackTrace();
                }
                break;

            case "insertar":
                try {
                    empBean.setNombre(request.getParameter("nombre"));
                    empBean.setPrimerApellido(request.getParameter("primerApellido"));
                    empBean.setSegundoApellido(request.getParameter("segundoApellido"));
                    empBean.setFechaN(request.getParameter("fechaNacimiento"));
                    empBean.setIdDep(new DepartamentosBean(request.getParameter("departamento")));
                    Part part = request.getPart("foto");
                    InputStream foto = part.getInputStream();
                    empBean.setFoto(foto);
                    if (emp.insertEmp(empBean)){
                        List<DepartamentosBean> Listadep = emp.searchDep();
                        request.setAttribute("listDep",Listadep);
                        request.getRequestDispatcher("/views/registrar.jsp").forward(request,response);
                    }
                }catch (Exception e){
                    e.printStackTrace();
                }
                break;
        }
    }

    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");
        request.setCharacterEncoding("UTF-8");
        String accion = request.getParameter("accion");

        EmpleadosBean empBean = new EmpleadosBean();
        EmpleadosDao emp = new EmpleadosDao();

        switch (accion){
            case "registrar":
                try{
                    List<DepartamentosBean> dep = emp.searchDep();
                    request.setAttribute("listDep",dep);
                    request.getRequestDispatcher("/views/registrar.jsp").forward(request,response);
                }catch (Exception e){
                    e.printStackTrace();
                }
                break;

            case "consultar":
                try{
                    List<DepartamentosBean> dep = emp.searchDep();
                    request.setAttribute("listDep",dep);
                    List<EmpleadosBean> empList = emp.searchEmp();
                    request.setAttribute("listEmp",empList);
                    request.getRequestDispatcher("/views/consultar.jsp").forward(request,response);
                }catch (Exception e){
                    e.printStackTrace();
                }
                break;

            case "delete":
                try{
                    empBean.setId(Integer.parseInt(request.getParameter("idEmp")));
                    emp.deleteEmp(empBean);
                    List<DepartamentosBean> dep = emp.searchDep();
                    request.setAttribute("listDep",dep);
                    List<EmpleadosBean> empList = emp.searchEmp();
                    request.setAttribute("listEmp",empList);
                    request.getRequestDispatcher("/views/consultar.jsp").forward(request,response);
                }catch (Exception e){
                    e.printStackTrace();
                }
                break;
            case "verFoto":
                try{
                    empBean.setId(Integer.parseInt(request.getParameter("idEmp")));
                    emp.selectImagen(empBean, response);
                }catch (Exception e){
                    e.printStackTrace();
                }
                break;
        }

    }
}
