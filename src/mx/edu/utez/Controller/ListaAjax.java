package mx.edu.utez.Controller;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import mx.edu.utez.Model.Bean.EmpleadosBean;
import mx.edu.utez.Model.Dao.EmpleadosDao;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.ArrayList;

@WebServlet("/ListaAjax")
public class ListaAjax extends HttpServlet {
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        EmpleadosBean emp = new EmpleadosBean();
        emp.setId(Integer.parseInt(request.getParameter("id")));
        EmpleadosDao empleadosDao = new EmpleadosDao();
        if (empleadosDao.deleteEmp(emp)){
            System.out.println("Si se borro jajaja");
        }else {
            System.out.println("No se borro nada jsjsjsjs");
        }
    }

    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        System.out.println("Si llega");
        Gson g = new Gson();
        String jsonList = g.toJson(new EmpleadosDao().searchEmp(), new TypeToken<ArrayList<EmpleadosBean>>() {}.getType());
        PrintWriter writer = response.getWriter();
        response.setContentType("application/json");
        response.setCharacterEncoding("UTF-8");
        writer.write(jsonList);
        writer.close();
    }
}
